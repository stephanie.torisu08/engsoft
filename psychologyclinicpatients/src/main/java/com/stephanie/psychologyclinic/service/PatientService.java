package com.stephanie.psychologyclinic.service;

import com.stephanie.psychologyclinic.model.Patient;
import com.stephanie.psychologyclinic.repository.PatientRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class PatientService {

    @Autowired
    private PatientRepository patientRepository;

    public Patient createPatient (Patient patient){
        return patientRepository.save(patient);
    }

    public List<Patient> listAllPatients(){
        return patientRepository.findAll();
    }
    public ResponseEntity<Patient> getPatientById(Long id){
        return patientRepository.findById(id)
                .map(patient -> ResponseEntity.ok().body(patient))
                .orElse(ResponseEntity.notFound().build());
    }
    public ResponseEntity<Object> deletePatientById(Long id){
        return patientRepository.findById(id)
                .map(patientToDelete -> {
                    patientRepository.deleteById(id);
                    return ResponseEntity.noContent().build();
                }).orElse(ResponseEntity.notFound().build());
    }
    public ResponseEntity<Patient> updatePatientById(Patient patient, Long id){
        return patientRepository.findById(id)
                .map(patientToUptade -> {
                    patientToUptade.setName(patient.getName());
                    patientToUptade.setBirthday(patient.getBirthday());
                    Patient updated = patientRepository.save(patientToUptade);
                    return ResponseEntity.ok().body(updated);
                }).orElse(ResponseEntity.notFound().build());
    }
}
