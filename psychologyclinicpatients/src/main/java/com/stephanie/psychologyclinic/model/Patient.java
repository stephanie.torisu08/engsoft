package com.stephanie.psychologyclinic.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "patients")
@Setter
@Getter
@ToString
public class Patient implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idPatient;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String birthday;

}
