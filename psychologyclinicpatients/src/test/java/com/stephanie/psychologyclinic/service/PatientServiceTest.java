package com.stephanie.psychologyclinic.service;

import com.stephanie.psychologyclinic.AppConfigTest;
import com.stephanie.psychologyclinic.model.Patient;
import com.stephanie.psychologyclinic.repository.PatientRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Optional;

@DisplayName("PatientServiceTest")
public class PatientServiceTest extends AppConfigTest {

    @MockBean
    private PatientRepository patientRepository;
    @Autowired
    private PatientService patientService;

    @Mock
    private AutoCloseable autoCloseable;

    @BeforeEach
    private void init(){
        autoCloseable = MockitoAnnotations.openMocks(this);
    }

    @AfterEach
    public void closeService() throws Exception{
        autoCloseable.close();
    }

    @Test
    @DisplayName("Should create patient")
    public void shouldCreatePatient() {
        Patient patientMock = Mockito.mock(Patient.class);
        Mockito.when(patientRepository.save(ArgumentMatchers.eq(patientMock))).thenReturn(patientMock);

        patientService.createPatient(patientMock);

        Mockito.verify(patientRepository,Mockito.times(1)).save(patientMock);
    }

    @Test
    @DisplayName("Should remove patient")
    public void shouldRemovePatient() {
        Long patientId = 1L;
        Patient patientMock = Mockito.mock(Patient.class);
        Mockito.when(patientRepository.findById(ArgumentMatchers.eq(patientId))).thenReturn(Optional.ofNullable(patientMock));

        patientService.deletePatientById(patientId);

        Mockito.verify(patientRepository,Mockito.times(1)).deleteById(ArgumentMatchers.eq(patientId));
    }

    @Test
    @DisplayName("Should update patient")
    public void shouldUpdatePatient() {
        Patient patient = Mockito.mock(Patient.class);
        Patient patientToUpdateMock = Mockito.mock(Patient.class);
        Mockito.when(patientRepository.findById(ArgumentMatchers.eq(patientToUpdateMock.getIdPatient()))).thenReturn(Optional.ofNullable(patientToUpdateMock));
        Mockito.when(patient.getName()).thenReturn("Name");
        Mockito.when(patient.getBirthday()).thenReturn("Birthday");

        patientService.updatePatientById(patient,patientToUpdateMock.getIdPatient());

        Mockito.verify(patientRepository,Mockito.times(1)).save(ArgumentMatchers.any(Patient.class));
    }
}
