package com.stephanie.psychologyclinic.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.stephanie.psychologyclinic.AppConfigTest;
import com.stephanie.psychologyclinic.model.Patient;
import com.stephanie.psychologyclinic.service.PatientService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.mockito.Mockito.mock;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@DisplayName("PatientControllerTest")
public class PatientControllerTest extends AppConfigTest {
    @MockBean
    PatientService patientService;

    @Autowired
    private MockMvc mockMvc;

    private static final String PATH = "/api/v1";


}
